﻿using Minrex.Privilegios.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Minrex.Privilegios.Controllers
{
    public class PrivilegiosController : Controller
    {
        // GET: Privilegios

        
        public ActionResult CrearAcreditacion(f02AcreditacionCD Acreditacion, HttpPostedFileBase File)
        {
           SqlConnection cnnPrivilegios = null;
           SqlCommand cmdAcreditaciones = null;
           ConnectionStringSettings connString = null;

            if (Acreditacion == null)
            {
                List<string> errors = new List<string>();
                errors.Add("No se transfirieron bien los datos hacia el servidor.");

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, String.Join("  ", errors));

            }
            
            DataTable dt = new DataTable();
            dt.Columns.Add("Fecha", typeof(DateTime));
            dt.Columns.Add("Entidad", typeof(string));
            dt.Columns.Add("Vocativo", typeof(string));
            dt.Columns.Add("Nombre", typeof(string));
            dt.Columns.Add("Apellido", typeof(string));
            dt.Columns.Add("Pasaporte", typeof(string));
            dt.Columns.Add("Pais", typeof(string));
            dt.Columns.Add("Cargo", typeof(string));
            dt.Columns.Add("FechaInicio", typeof(DateTime));
            dt.Columns.Add("CorreoE", typeof(string));
            dt.Columns.Add("Nota", typeof(byte[]));
            dt.Columns.Add("NotaFile", typeof(string));
            dt.Columns.Add("NotaTipo", typeof(string));
            dt.Columns.Add("NotaSize", typeof(int));


            if (ModelState.IsValid)
            {
                DataRow row = dt.NewRow();
                row["Fecha"] = Acreditacion.Fecha;
                row["Entidad"] = Acreditacion.Entidad;
                row["Vocativo"] = Acreditacion.Vocativo;
                row["Nombre"] = Acreditacion.Nombre;
                row["Apellido"] = Acreditacion.Apellido;
                row["Pasaporte"] = Acreditacion.Pasaporte;
                row["Pais"] = Acreditacion.Pais;
                row["Cargo"] = Acreditacion.Cargo;
                row["FechaInicio"] = Acreditacion.FechaInicio;
                row["CorreoE"] = Acreditacion.CorreoE;
                /*                row["Nota"]
                                row["NotaFiles"]
                                row["NotaTip"]
                 */
                dt.Rows.Add(row);
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Fallo el registro de la solicitud.");

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, String.Join("  ", errors));

            }

           Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/");

            if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
            {
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ServiciosCiudadania"];
                if (connString != null)
                {
                    //Constring esta correcto!
                    //ViewBag.URLReporte3 = connString.ToString();
                }
                else
                {
                    //Constring esta incorrecto!
                    List<string> errors = new List<string>();
                    errors.Add("Fallo el registro de la solicitud.");

                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, String.Join("  ", errors));

                }
            }

            // string queryString = "   ";

            try
            {
                using (cnnPrivilegios = new SqlConnection(connString.ConnectionString))
                {
                    cnnPrivilegios.Open();

                    using (cmdAcreditaciones = new SqlCommand("frmAcreditaciones", cnnPrivilegios))
                    {
                        cmdAcreditaciones.CommandType = CommandType.StoredProcedure;
                        cmdAcreditaciones.CommandText = "dbo.frmAcreditaciones";
                        SqlParameter param = cmdAcreditaciones.Parameters.AddWithValue("@Solicitud", dt);
                        int respuesta = cmdAcreditaciones.ExecuteNonQuery();

                    }
                }
            }
            catch (InvalidCastException e)
            {
                //Console.WriteLine("dio Error de excepcion" + e.ToString());
                List<string> errors = new List<string>();
                errors.Add("Fallo el registro de la solicitud.");

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, String.Join("  ", errors));
            }

            finally
            {
               
                if (cmdAcreditaciones != null)
                    cmdAcreditaciones.Dispose();
                if (cnnPrivilegios != null)
                    cnnPrivilegios.Close();

            }
            return new HttpStatusCodeResult(HttpStatusCode.OK, "Su solicitud ha sido registrada.");

        }



        public ActionResult ListadoPaises()
        {
            SqlConnection cnnPrivilegios = null;
            SqlCommand cmdAcreditaciones = null;
            ConnectionStringSettings connString = null;
            



            List<Pais> Paises = new List<Pais>();


            Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/");

            if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
            {
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ServiciosCiudadania"];
                if (connString != null)
                {
                    //Constring esta correcto!
                    
                }
                else
                {
                    //Constring esta incorrecto!
                    List<string> errors = new List<string>();
                    errors.Add("Fallo en carga de paises.");

                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, String.Join("  ", errors));

                }
            }
           
            try
            {
                using (cnnPrivilegios = new SqlConnection(connString.ConnectionString))
                {
                    cnnPrivilegios.Open();
                    string queryString = "SELECT clave,descrip FROM clasif WHERE padre LIKE '10020%' ";

                    using (cmdAcreditaciones = new SqlCommand(queryString, cnnPrivilegios))
                    {
                        SqlDataReader drPrivilegios = cmdAcreditaciones.ExecuteReader(CommandBehavior.CloseConnection);
                        while (drPrivilegios.Read())
                        {
                            Paises.Add(new Pais {
                                Id = drPrivilegios["clave"].ToString(),
                                Nombre_Pais = drPrivilegios["descrip"].ToString()
                            });
                        }   


                        //int respuesta = cmdAcreditaciones.ExecuteNonQuery();

                    }
                }
            }
            catch (InvalidCastException e)
            {
                //Console.WriteLine("dio Error de excepcion" + e.ToString());
                List<string> errors = new List<string>();
                errors.Add("Fallo el registro de la solicitud.");

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, String.Join("  ", errors));
            }

            finally
            {
               
                if (cmdAcreditaciones != null)
                    cmdAcreditaciones.Dispose();
                if (cnnPrivilegios != null)
                    cnnPrivilegios.Close();
            }


//            **********
            /*{
            new Pais() {
                Id = 1,
                Nombre_Pais = "Panama"
                }
            };*/

            

            return ConvierteaJSONresult(Paises);
        }
        
        public ContentResult ConvierteaJSONresult(object data)
        {
            var camelCaseFormatter = new JsonSerializerSettings();
            camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var jsonResult = new ContentResult
            {
                Content = JsonConvert.SerializeObject(data, camelCaseFormatter),
                ContentType = "application/json"
            };
            return jsonResult;
        }

    }  
};

