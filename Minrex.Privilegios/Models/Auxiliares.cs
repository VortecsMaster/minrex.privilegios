﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Minrex.Privilegios.Models
{
    public class Pais
    {
        public string Id { get; set; }
        public string Nombre_Pais { get; set; }
             
        
    }

    public class Cargo
    {
        public string Id { get; set; }
        public string Nombre_Cargo { get; set; }
    }
}