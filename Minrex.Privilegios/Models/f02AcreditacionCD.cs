﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Minrex.Privilegios.Models
{
    public class f02AcreditacionCD
    {
        public DateTime Fecha { get; set; }
        public string Entidad { get; set; }

        public string Vocativo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Pasaporte { get; set; }
        public string Pais { get; set; }
        public string Cargo { get; set; }
        public DateTime FechaInicio { get; set; }
        public string CorreoE { get; set; }
        public byte[] Nota { get; set; }
        public string NotaFile { get; set; }
        public string NotaTipo { get; set; }
        public int NotaSize { get; set; }

        
    }
}