﻿privilegiosApp.Controller('f07CarnetIdController',
    ["$scope", "$window", "$routeParams", "DataService",
        function f07CarnetIdController($scope, $window, $routeParams, DataService) {
            $scope.solicitudes={};
        

            $scope.submitForm = function () {
                var result = DataService.PostDataCall('Privilegios','SolicitudCarnet',$scope.solicitudes).success(function (data)) {
                    $scope.message = data;
                    $window.history.back();
                });
            };

            $scope.cancelForm = function () {
                $window.history.back();
            };

            $scope.resetForm = function () {
                //$scope.$broadcast('hide-errors-event');
            };

        }]);