﻿privilegiosApp.service('DataService', ["$http", function ($http) {
    var result;

    this.GetDataCall = function (nombreControlador, nombreMetodo) {
        result = $http.get('/' + nombreControlador + '/' + nombreMetodo).success(function (data, status) {
            result = (data);
        }).error(function () {
            alert("Algo salio mal...");
        });
        return result;
    };

    this.PostDataCall = function (nombreControlador, nombreMetodo,obj) {
        result = $http.post('/' + nombreControlador + '/' + nombreMetodo, obj).success(function (data, status) {
            result = (data);
        }).error(function () {
            alert("Algo salio mal..."+ status);
        });
        return result;
    };
}]);

