﻿var privilegiosApp = angular.module('privilegiosApp', ["ngRoute", 'ui.bootstrap','ngAnimate']);

privilegiosApp.config(["$routeProvider", "$locationProvider",
    function ($routeProvider, $locationProvider) {
        $routeProvider
        .when("/testMenu", {
            templateUrl: "app/testMenu.html",
            controller: "testMenuController"
        })
        .when("/acreditaciones", {
            templateUrl: "app/f02Acreditacion/f02Template.html",
            controller: "f02AcreditacionController"
        })
        .when("/carnetId", {
            templateUrl: "app/f07CarnetId/f07Template.html",
            controller: "f07CarnetIdController"
        })
        .when("/certificacion", {
            templateUrl: "app/f11Certificacion/f11Template.html",
            controller: "f11CertificacionController"
        })
        .when("/exeCombustible", {
            templateUrl: "app/f16ExeCombustible/f16Template.html",
            controller: "f16ExeCombustibleController"
        })
        .when("/exeImpuesto", {
            templateUrl: "app/f17ExeImpuesto/f17Template.html",
            controller: "f17ExeImpuestoController"
        })
        .when("/aero", {
            templateUrl: "app/f18Aero/f18Template.html",
            controller: "f18AeroController"
        })
        .when("/franquicia", {
            templateUrl: "app/f21Franquicia/f21Template.html",
            controller: "f21FranquiciaController"
        })
        .when("/rexpor", {
            templateUrl: "app/f26Rexpor/f26Template.html",
            controller: "f26RexporController"
        })
        .when("/esvehiculos", {
            templateUrl: "app/f27ESvehiculos/f27Template.html",
            controller: "f27ESvehiculosController"
        })
        .when("/ventavehiculos", {
            templateUrl: "app/f28VentaVehiculos/f28Template.html",
            controller: "f28VentaVehiculosController"
        })
        .when("/permisotrabajo", {
            templateUrl: "app/f29PermisoTrabajo/f29Template.html",
            controller: "f29PermisoTrabajoController"
        })
        .when("/placa", {
            templateUrl: "app/f33Placa/f33Template.html",
            controller: "f33PlacaController"
        })
        .when("/visa", {
            templateUrl: "app/f36Visa/f36Template.html",
            controller: "f36VisaController"
        })
        .when("/exeArrendamiento", {
            templateUrl: "app/f37ExeArrendamiento/f37Template.html",
            controller: "f37ExeArrendamientoController"
        })
        .when("/liberaPlaca", {
            templateUrl: "app/f38LiberaPlaca/f38Template.html",
            controller: "f38LiberaPlacaController"
        })
        .otherwise({
            redirectTo: "/testMenu"
          
        });

    }]);

privilegiosApp.controller("testMenuController",
    ["$scope", "$location", "DataService",
    function ($scope,$location,DataService) {
            
        $scope.showSolicitarAcreditacion = function () {
            $location.path('/acreditaciones');
        };

        $scope.showSolicitarCarnet = function () {
            $location.path('/carnetId');
        };
    }]);